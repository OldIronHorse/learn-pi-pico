from setuptools import setup

def readme():
  with open('README.md') as f:
    return f.read()

setup(name='learn-pi-pico',
    version='0.1',
    description='Mock modules for automated testing of Pi Pico scripts',
    long_description=readme(),
    keywords='',
    url='http://gitlab.com/OldIronHorse/learn-pi-pico',
    author='Simon Redding',
    author_email='s1m0n.r3dd1ng@gmail.com',
    license='GPL3',
    packages=[
        'src',
        ],
    python_requires='>=3.10.0',
    install_requires=[
        'mockpipico',
        ],
    scripts=[
        ],
    tests_require=[
        'pytest',
        'pytest-mock',
        'pytest-watch',
        ],
    include_package_data=True,
    zip_safe=False)
