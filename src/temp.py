from machine import ADC, Timer


class Temperature:
    def __init__(self):
        self.sensor = ADC(4)
        self.timer = Timer()
        self.temp = 0

    def start(self, sample_freq):
        self._sample(None)
        self.timer.init(freq=sample_freq, mode=Timer.PERIODIC,
                        callback=self._sample)

    def _sample(self, timer):
        TEMP_CONVERSION = 3.3 / (65535)
        reading = self.sensor.read_u16() * TEMP_CONVERSION
        self.temp = 27 - (reading - 0.706)/0.001721
