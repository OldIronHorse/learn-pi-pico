from machine import Pin, I2C, Timer
from time import sleep_ms, ticks_ms, ticks_diff


class DeviceNotFoundError(Exception):
    def __init__(self, address):
        super().__init__()
        self.address = address


class Nunchuk:
    ADDRESS = 0x52
    JOYSTICK_X_MIN = 34
    JOYSTICK_X_CENTRE = 125
    JOYSTICK_X_MAX = 223
    JOYSTICK_Y_MIN = 30
    JOYSTICK_Y_CENTRE = 130
    JOYSTICK_Y_MAX = 220

    def __init__(self, i2c_number, scl_number, sda_number):
        self.j_x = 0
        self.j_y = 0
        self.i2c_number = i2c_number
        self.scl = Pin(scl_number)
        self.sda = Pin(sda_number)
        self.i2c = I2C(i2c_number, sda=self.sda, scl=self.scl,
                       freq=100000)
        if Nunchuk.ADDRESS not in self.i2c.scan():
            raise DeviceNotFoundError(Nunchuk.ADDRESS)
        self.buffer = bytearray(b'\x00\x00\x00\x00\x00\x00')
        sleep_ms(50)
        #self.i2c.writeto(Nunchuk.ADDRESS, b'\x40\x00')
        #self.i2c.writeto(Nunchuk.ADDRESS, b'\x00')
        self.i2c.writeto(Nunchuk.ADDRESS, b'\xf0\x55')
        self.i2c.writeto(Nunchuk.ADDRESS, b'\xfb\x00')
        sleep_ms(50)
        self.i2c.writeto(Nunchuk.ADDRESS, b'\x00')
        self.last_write_ms = ticks_ms()

    def _update(self):
        if ticks_diff(ticks_ms(), self.last_write_ms) > 50:
            self.i2c.readfrom_into(Nunchuk.ADDRESS, self.buffer)
            self.i2c.writeto(Nunchuk.ADDRESS, b'\x00')
            self.last_write_ms = ticks_ms()

    def joystick(self):
        self._update()
        if self.buffer[0] != 0 and self.buffer[1] !=0:
            x = self.buffer[0] - Nunchuk.JOYSTICK_X_CENTRE
            y = self.buffer[1] - Nunchuk.JOYSTICK_Y_CENTRE
            self.j_x = x if abs(x) > 5 else 0
            self.j_y = y if abs(y) > 5 else 0
        return self.j_x, self.j_y

    def accelerometer(self):
        self._update()
        return ((self.buffer[2] << 2) + ((self.buffer[5] << 4) >> 6),
                (self.buffer[3] << 2) + ((self.buffer[5] << 2) >> 6),
                (self.buffer[4] << 2) + (self.buffer[5] >> 6))

    def button_c(self):
        self._update()
        return not (self.buffer[5] & 2 == 2)

    def button_z(self):
        self._update()
        return not (self.buffer[5] & 1 == 1)

def test():
    n = Nunchuk(0, 17, 16)
    while True:
        print('joystick:', n.joystick())
        sleep_ms(1000)
