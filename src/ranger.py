from machine import Pin
from utime import sleep_us, ticks_us


class Ranger:
    def __init__(self, trig_pin, echo_pin):
        self.echo = Pin(echo_pin, Pin.IN)
        self.trig = Pin(trig_pin, Pin.OUT)
        self.st = 0
        self.range = 0
        self.echo.irq(trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING,
                      handler=self._echo_start_stop)

    def _echo_start_stop(self, pin):
        if self.st == 0:
            self.st = ticks_us()
        else:
            self.range = (ticks_us() - self.st) * 0.031594 / 2
            self.st = 0

    def measure(self):
        self.trig.high()
        sleep_us(10)
        self.trig.low()
        self.st = 0
