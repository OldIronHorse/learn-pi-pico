from utime import sleep_us
from nunchuk import Nunchuk
from mearm import MeArm


HOME = (MeArm.Position.ELBOW_MIN, MeArm.Position.SHOULDER_MIN, 4800)


def run():
    arm = MeArm(15, 14, 13, 12)
    arm.pos.set_grab(MeArm.Position.GRAB_OPEN)
    arm.pos.set_elbow(MeArm.Position.ELBOW_MIN)
    arm.pos.set_shoulder(MeArm.Position.SHOULDER_MIN)
    arm.pos.set_pan(4800)
    arm.to_pos()
    n = Nunchuk(0, 17, 16)
    while True:
        if n.button_z():
            arm.pos.set_grab(MeArm.Position.GRAB_CLOSED)
        else:
            arm.pos.set_grab(MeArm.Position.GRAB_OPEN)
        x, y = n.joystick()
        arm.pos.set_pan(arm.pos.pan + int(x * -1 / 4))
        arm.to_pos()
        sleep_us(500)
