from machine import Pin, PWM, Timer


class MeArm:
    STEP = 5
    DT = 0.01

    class Position:
        GRAB_CLOSED = 7600
        GRAB_OPEN = 6500
        ELBOW_MIN = 4000
        ELBOW_MAX = 7000
        SHOULDER_MIN = 5500
        SHOULDER_MAX = 7500
        PAN_MIN = 2000
        PAN_MAX = 8000

        def __init__(self, grab, elbow, shoulder, pan):
            self.set_grab(grab)
            self.set_elbow(elbow)
            self.set_shoulder(shoulder)
            self.set_pan(pan)

        def set_grab(self, pos):
            self.grab = max(min(pos, MeArm.Position.GRAB_CLOSED),
                            MeArm.Position.GRAB_OPEN)

        def set_pan(self, pos):
            self.pan = max(min(pos, MeArm.Position.PAN_MAX),
                           MeArm.Position.PAN_MIN)

        def set_shoulder(self, pos):
            self.shoulder = max(min(pos, MeArm.Position.SHOULDER_MAX),
                                MeArm.Position.SHOULDER_MIN)

        def set_elbow(self, pos):
            self.elbow = max(min(pos, MeArm.Position.ELBOW_MAX),
                             MeArm.Position.ELBOW_MIN)

    def __init__(self, grab_pin, elbow_pin, shoulder_pin, pan_pin):
        self.pos = MeArm.Position(MeArm.Position.GRAB_OPEN, 4000,
                                  MeArm.Position.SHOULDER_MIN, 4800)
        self.targets = []
        self.grab = PWM(Pin(grab_pin))
        self.grab.freq(50)
        self.grab.duty_u16(self.pos.grab)
        self.elbow = PWM(Pin(elbow_pin))
        self.elbow.freq(50)
        self.elbow.duty_u16(self.pos.elbow)
        self.shoulder = PWM(Pin(shoulder_pin))
        self.shoulder.freq(50)
        self.shoulder.duty_u16(self.pos.shoulder)
        self.pan = PWM(Pin(pan_pin))
        self.pan.freq(50)
        self.pan.duty_u16(self.pos.pan)
        self.dt = 0.001
        self.step_timer = Timer()
        self.v_pan = 0
        self.speed_timer = Timer()

    def start(self):
        self.speed_timer.init(mode=Timer.PERIODIC, freq=100,
                              callback=self._speed_step)

    def stop(self):
        self.speed_timer.deinit()

    def is_moving(self):
        return bool(self.targets)

    def _do_step(self, timer):
        if self.targets:
            if self.pos.grab < self.targets[0].grab:
                self.pos.grab += 1
            elif self.pos.grab > self.targets[0].grab:
                self.pos.grab -= 1
            if self.pos.elbow < self.targets[0].elbow:
                self.pos.elbow += 1
            elif self.pos.elbow > self.targets[0].elbow:
                self.pos.elbow -= 1
            if self.pos.shoulder < self.targets[0].shoulder:
                self.pos.shoulder += 1
            elif self.pos.shoulder > self.targets[0].shoulder:
                self.pos.shoulder -= 1
            if self.pos.pan < self.targets[0].pan:
                self.pos.pan += 1
            elif self.pos.pan > self.targets[0].pan:
                self.pos.pan -= 1
            self.to_pos()
            if (self.pos.grab == self.targets[0].grab
                    and self.pos.elbow == self.targets[0].elbow
                    and self.pos.shoulder == self.targets[0].shoulder
                    and self.pos.pan == self.targets[0].pan):
                self.targets.pop(0)
        else:
            self.step_timer.deinit()
            pass

    def to_pos(self):
        self.grab.duty_u16(self.pos.grab)
        self.elbow.duty_u16(self.pos.elbow)
        self.shoulder.duty_u16(self.pos.shoulder)
        self.pan.duty_u16(self.pos.pan)

    def _move(self, target):
        self.targets.append(target)
        self.step_timer.init(mode=Timer.PERIODIC, freq=1000,
                             callback=self._do_step)

    def _from(self):
        return self.targets[-1] if self.targets else self.pos

    def grab_to(self, target):
        f = self._from()
        self._move(MeArm.Position(target, f.elbow, f.shoulder, f.pan))

    def pan_to(self, target):
        f = self._from()
        self._move(MeArm.Position(f.grab, f.elbow, f.shoulder, target))

    def elbow_to(self, target):
        f = self._from()
        self._move(MeArm.Position(f.grab, target, f.shoulder, f.pan))

    def shoulder_to(self, target):
        f = self._from()
        self._move(MeArm.Position(f.grab, f.elbow, target, f.pan))

    def move_to(self, pos_3dof):
        elbow_target, shoulder_target, pan_target = pos_3dof
        f = self._from()
        self._move(MeArm.Position(f.grab, elbow_target, shoulder_target,
                                  pan_target))

    def pan_speed(self, v):
        self.v_pan = v

    def _speed_step(self, t):
        if not self.is_moving():
            self.pos.set_pan(self.pos.pan + self.v_pan)
            self.pan.duty_u16(self.pos.pan)
