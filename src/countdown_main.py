from button import Button
from lcd1602 import lcd1602
from machine import Timer


display = lcd1602(12, 11, 10, 9, 8, 7)
display.cursor(False)
display.clear()
tick_timer = Timer()
ticks = 0


def inc_ticks(t):
    global ticks
    ticks += 1

def on_start(t):
    global ticks
    ticks = 0
    tick_timer.init(mode=Timer.PERIODIC, freq=100, callback=inc_ticks)


def on_stop(t):
    tick_timer.deinit()


start = Button(0, on_start)
stop = Button(1, on_stop)

def render(t):
    display.position(0, 0)
    display.write(f'{ticks:05}')

render_timer = Timer()
render_timer.init(mode=Timer.PERIODIC, freq=10, callback=render)
