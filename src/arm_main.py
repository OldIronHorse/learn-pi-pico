#  LCD Pins
#  1. GND
#  2. VDD
#  3. V0
#  4. RS
#  5. RW
#  6. E
#  7. DB0
#  ...
# 14. DB7
# 15. BL1
# 16. BL2

from machine import Timer, Pin
from ranger import Ranger
from mearm import MeArm

from time import sleep


arm = MeArm(15, 14, 13, 12)
PAD_HOME = (MeArm.Position.ELBOW_MIN, MeArm.Position.SHOULDER_MIN, 4800)
PICKUP_HOME = (MeArm.Position.ELBOW_MIN, MeArm.Position.SHOULDER_MIN, 7500)

ranger = Ranger(17, 16)

action = None


def measure_range(timer):
    global action
    ranger.measure()
    if (not arm.is_moving()) and ranger.range and ranger.range < 10:
        action = 'to_pad'


measure_timer = Timer()
measure_timer.init(freq=2, mode=Timer.PERIODIC, callback=measure_range)


def move_to_pad():
    arm.move_to(PICKUP_HOME)
    arm.grab_to(MeArm.Position.GRAB_OPEN)
    arm.shoulder_to(7000)
    arm.elbow_to(5000)
    arm.grab_to(MeArm.Position.GRAB_CLOSED)
    arm.shoulder_to(MeArm.Position.SHOULDER_MIN)
    arm.move_to(PAD_HOME)
    arm.elbow_to(5000)
    arm.shoulder_to(7000)
    arm.grab_to(MeArm.Position.GRAB_OPEN)
    arm.elbow_to(4000)
    arm.shoulder_to(MeArm.Position.SHOULDER_MIN)


def move_from_pad():
    arm.move_to(PAD_HOME)
    arm.grab_to(MeArm.Position.GRAB_OPEN)
    arm.shoulder_to(7000)
    arm.elbow_to(5000)
    arm.grab_to(MeArm.Position.GRAB_CLOSED)
    arm.shoulder_to(MeArm.Position.SHOULDER_MIN)
    arm.move_to(PICKUP_HOME)
    arm.elbow_to(5000)
    arm.shoulder_to(7000)
    arm.grab_to(MeArm.Position.GRAB_OPEN)
    arm.elbow_to(4000)
    arm.shoulder_to(MeArm.Position.SHOULDER_MIN)


def from_pad(timer):
    global action
    if not arm.is_moving():
        action = 'from_pad'


debounce_timer = Timer()


def button_on_release(pin):
    global action
    debounce_timer.init(mode=Timer.ONE_SHOT, period=200, callback=from_pad)


button = Pin(18, Pin.IN, Pin.PULL_DOWN)
button.irq(trigger=Pin.IRQ_FALLING, handler=button_on_release)

try:
    while True:
        sleep(1)
        while not action:
            sleep(1)
        if action == 'to_pad':
            move_to_pad()
        elif action == 'from_pad':
            move_from_pad()
        action = None
except KeyboardInterrupt:
    pass
