#   LCD Pins
#   1. GND
#   2. VDD
#   3. V0
#   4. RS
#   5. RW
#   6. E
#   7. DB0
#   ...
#  14. DB7
#  15. BL1
#  16. BL2

from lcd1602 import lcd1602
from machine import ADC, Timer


PROGRESS = ['   ', '.  ', '.. ', '...']
progress_index = 0

display = lcd1602(12, 11, 10, 9, 8, 7)
display.cursor(False)
display.clear()

sensor_temp = ADC(4)


def get_temp(timer):
    global display, sensor_temp, PROGRESS, progress_index
    TEMP_CONVERSION = 3.3 / (65535)
    reading = sensor_temp.read_u16() * TEMP_CONVERSION
    temp = 27 - (reading - 0.706)/0.001721
    display.position(1, 11)
    display.write(f'{temp:.1f}C')
    display.position(0, 0)
    display.write(PROGRESS[progress_index])
    progress_index += 1
    if progress_index >= len(PROGRESS):
        progress_index = 0


timer_temp = Timer()
timer_temp.init(freq=0.2, mode=Timer.PERIODIC, callback=get_temp)

get_temp(None)
