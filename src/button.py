from machine import Pin, Timer


class Button:
    def __init__(self, pin_number, callback, on_release=True):
        self.callback = callback
        self.pin = Pin(pin_number, Pin.IN, Pin.PULL_DOWN)
        trigger = Pin.IRQ_FALLING if on_release else Pin.IRQ_RISING
        self.pin.irq(trigger=trigger, handler=self._on_release)
        self.debounce = Timer()

    def _on_release(self, pin):
        self.debounce.init(mode=Timer.ONE_SHOT, period=200,
                           callback=self.callback)
