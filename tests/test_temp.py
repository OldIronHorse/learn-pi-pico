import pytest
from src.temp import Temperature
from machine import Timer
from machine import ADC

def test_temperature():
    t = Temperature()
    ADC.by_number[4].t_input_value(14000)
    t.start(0.2)
    ADC.by_number[4].t_input_value(14500)

    assert t.temp == pytest.approx(27, 1)

    Timer.t_tick(3000)

    assert t.temp == pytest.approx(27, 1)

    Timer.t_tick(3000)

    assert t.temp == pytest.approx(13, 1)
