import pytest
from mockpipico import eventlog
from machine import Pin
from utime import sleep_us
from src.ranger import Ranger


@pytest.fixture
def events():
    eventlog.reset()
    return eventlog.events


def test_trigger(events):
    r = Ranger(13, 14)
    r.measure()

    assert events == [(0, (Pin, 13, Pin.OUT, True)),
                      (10, (Pin, 13, Pin.OUT, False))]


def test_echo(events):
    r = Ranger(13, 14)
    r.measure()
    sleep_us(12)    # wait for echo

    Pin.by_number[14].t_input_value(True)
    eventlog.ticks_us += 1234
    Pin.by_number[14].t_input_value(False)

    assert r.range == pytest.approx(19.49, 0.001)
