from unittest.mock import MagicMock
from machine import Pin, Timer
from src.button import Button


def test_click():
    cb = MagicMock()
    Button(15, cb)

    Pin.by_number[15].t_input_value(True)

    Timer.t_tick(500)
    cb.assert_not_called()

    Pin.by_number[15].t_input_value(False)

    Timer.t_tick(500)
    cb.assert_called_once()


def test_click_on_push():
    cb = MagicMock()
    Button(15, cb, on_release=False)

    Pin.by_number[15].t_input_value(True)

    Timer.t_tick(500)
    cb.assert_called_once()

    Pin.by_number[15].t_input_value(False)

    Timer.t_tick(500)
    cb.assert_called_once()


def test_click_debounce():
    cb = MagicMock()
    Button(15, cb)

    Pin.by_number[15].t_input_value(True)

    Timer.t_tick(50)
    Pin.by_number[15].t_input_value(False)
    Pin.by_number[15].t_input_value(True)
    Timer.t_tick(50)
    Pin.by_number[15].t_input_value(False)
    Pin.by_number[15].t_input_value(True)
    Timer.t_tick(50)
    Pin.by_number[15].t_input_value(False)
    Pin.by_number[15].t_input_value(True)

    cb.assert_not_called()

    Timer.t_tick(500)
    cb.assert_called_once()
